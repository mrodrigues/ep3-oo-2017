Deface::Override.new(:virtual_path => 'spree/shared/_footer',
	:name => 'change footer phone number',
	:replace => 'div.phone',
	:text => '<div class="phone">
				(55) 5555-5555
			  </div>'
)

Deface::Override.new(:virtual_path => 'spree/shared/_footer',
	:name => 'change footer email',
	:replace => 'div.email',
	:text => '<div class="email">
				sportshop@sportshop.com
			  </div>'
)

Deface::Override.new(:virtual_path => 'spree/shared/_footer',
	:name => 'change footer address',
	:replace => 'div.address',
	:text => '<div class="address">
				Gama FGA
			  </div>'
)